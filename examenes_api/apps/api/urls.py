from django.urls import path

from .views import *

urlpatterns = [
    path('usuarios/', api_usuarios, name='api_usuarios'),
    path('usuarios/<int:pk>', api_usuarios, name='api_usuarios'),
    path('usuarios/auth', api_usuarios_auth, name='api_usuarios_auth'),
    path('grupos/', api_grupos),
    path('grupos/<int:pk>', api_grupos),
    path('examenes/preguntas', api_preguntas),
    path('examenes/preguntas/<int:pk>', api_preguntas),
    path('examenes/', api_examenes),
    path('examenes/<int:pk>', api_examenes),
    path('examenes/iniciar-examen/<int:pk>', api_examenes_extra),
    path('examenes/terminar-examen/<int:pk>', api_examenes_extra),
    path('examenes/responder-examen/<int:pk>', api_examenes_extra),
    path('examenes/examenes-estudiante', api_examenes_extra),
    path('examenes/calificacion-examen/<int:pk>', api_examenes_extra),
    path('examenes/calificaciones-examen/<int:pk>', api_examenes_extra),
]
