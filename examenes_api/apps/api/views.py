import json

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django_microservices.models import MicroService
from rest_framework import status


@csrf_exempt
def api_usuarios(request, pk=None):
    microservicio = MicroService.objects.get(name="usuarios")
    respuesta = {}
    try:
        if request.method == "GET":
            if pk:
                respuesta = microservicio.remote_call(
                        request.method, api='api/usuarios/%s/' % pk, request=request
                )
            else:
                respuesta = microservicio.remote_call(
                        request.method, api='api/usuarios/', request=request
                )
        elif request.method == "POST":

            if pk:
                respuesta = microservicio.remote_call(
                        "PUT", api='api/usuarios/%s/' % pk, data=request.POST.dict(), files=request.FILES, request=request
                )
            else:
                respuesta = microservicio.remote_call(
                        request.method, api='api/usuarios/', data=request.POST.dict(), files=request.FILES, request=request
                )
        else:
            JsonResponse({}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception:
        return JsonResponse({}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return JsonResponse(respuesta.json(), status=respuesta.status_code, safe=False)


@csrf_exempt
def api_usuarios_auth(request):
    microservicio = MicroService.objects.get(name="usuarios")
    respuesta = {}

    try:
        if request.method == "POST":
            respuesta = microservicio.remote_call(
                    request.method, api='api/usuarios/auth', data=request.POST.dict()
            )
        else:
            JsonResponse({}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception:
        return JsonResponse({}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return JsonResponse(respuesta.json(), status=respuesta.status_code, safe=False)


@csrf_exempt
def api_grupos(request, pk=None):
    microservicio = MicroService.objects.get(name="grupos")
    respuesta = {}
    try:
        if request.method == "GET":
            if pk:
                respuesta = microservicio.remote_call(
                        request.method, api='api/grupos/%s' % pk, request=request
                )
            else:
                respuesta = microservicio.remote_call(
                        request.method, api='api/grupos/', request=request
                )
        elif request.method == "POST":

            if pk:
                respuesta = microservicio.remote_call(
                        "PUT", api='api/grupos/%s/' % pk, data=request.POST.dict(), files=request.FILES, request=request
                )
            else:
                respuesta = microservicio.remote_call(
                        request.method, api='api/grupos/', data=request.POST.dict(), files=request.FILES, request=request
                )
        else:
            JsonResponse({}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return JsonResponse({}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return JsonResponse(respuesta.json(), status=respuesta.status_code, safe=False)


@csrf_exempt
def api_preguntas(request, pk=None):
    microservicio = MicroService.objects.get(name="examenes")
    respuesta = {}
    if request.method == "POST":
        data = request.body.decode('utf-8')

    try:
        if request.method == "GET":
            if pk:
                respuesta = microservicio.remote_call(
                        request.method, api='api/examenes/preguntas/%s/' % pk, request=request
                )
            else:
                respuesta = microservicio.remote_call(
                        request.method, api='api/examenes/preguntas', request=request
                )
        elif request.method == "POST":

            if pk:
                respuesta = microservicio.remote_call(
                        "PUT", api='api/examenes/preguntas/%s/' % pk, json=json.loads(data), files=request.FILES, request=request
                )
            else:
                respuesta = microservicio.remote_call(
                        request.method, api='api/examenes/preguntas', json=json.loads(data), files=request.FILES, request=request
                )
        else:
            JsonResponse({}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return JsonResponse({}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return JsonResponse(respuesta.json(), status=respuesta.status_code, safe=False)


@csrf_exempt
def api_examenes(request, pk=None):
    microservicio = MicroService.objects.get(name="examenes")
    respuesta = {}
    data = []
    if request.method == "POST":
        data = request.body.decode('utf-8')

    try:
        if request.method == "GET":
            if pk:
                respuesta = microservicio.remote_call(
                        request.method, api='api/examenes/%s/' % pk, request=request
                )
            else:
                respuesta = microservicio.remote_call(
                        request.method, api='api/examenes/', request=request
                )
        elif request.method == "POST":

            if pk:
                respuesta = microservicio.remote_call(
                        "PUT", api='api/examenes/%s/' % pk, json=json.loads(data), files=request.FILES, request=request
                )
            else:
                respuesta = microservicio.remote_call(
                        request.method, api='api/examenes/', json=json.loads(data), files=request.FILES, request=request
                )
        else:
            JsonResponse({}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    except Exception as e:
        return JsonResponse({}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return JsonResponse(respuesta.json(), status=respuesta.status_code, safe=False)


@csrf_exempt
def api_examenes_extra(request, pk=None):
    microservicio = MicroService.objects.get(name="examenes")
    respuesta = {}
    data = request.body.decode('utf-8') if request.method == "POST" else {}

    try:
        if request.method == "GET" and pk:
            if 'iniciar-examen' in request.path:
                respuesta = microservicio.remote_call(
                        request.method, api='api/examenes/iniciar-examen/%s' % pk, request=request
                )
            elif 'terminar-examen' in request.path:
                respuesta = microservicio.remote_call(
                        request.method, api='api/examenes/terminar-examen/%s' % pk, request=request
                )
            elif 'responder-examen' in request.path:
                respuesta = microservicio.remote_call(
                        request.method, api='api/examenes/responder-examen/%s' % pk, request=request
                )
            elif 'calificacion-examen' in request.path:
                respuesta = microservicio.remote_call(
                        request.method, api='api/examenes/calificacion-examen/%s' % pk, request=request
                )
            elif 'calificaciones-examen' in request.path:
                respuesta = microservicio.remote_call(
                        request.method, api='api/examenes/calificaciones-examen/%s' % pk, request=request
                )
            else:
                return JsonResponse({}, status=status.HTTP_404_NOT_FOUND)
        elif request.method == "GET":
            if 'examenes-estudiante' in request.path:
                respuesta = microservicio.remote_call(
                        request.method, api='api/examenes/examenes-estudiante', request=request
                )
            else:
                return JsonResponse({}, status=status.HTTP_404_NOT_FOUND)
        elif request.method == "POST":
            if 'responder-examen' in request.path:
                if request.is_ajax():
                    headers = {'X-Requested-With': 'XMLHttpRequest'}
                else:
                    headers = {}

                respuesta = microservicio.remote_call(
                        "PUT", api='api/examenes/responder-examen/%s' % pk, json=json.loads(data), request=request, headers=headers
                )
            else:
                return JsonResponse({}, status=status.HTTP_404_NOT_FOUND)

        else:
            return JsonResponse({}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return JsonResponse({}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return JsonResponse(respuesta.json(), status=respuesta.status_code, safe=False)
